# LibcarePlus

#### Description
LibcarePlus‧is‧a‧general‧user-mode‧live‧patch‧framework.‧It delivers live patches to any of your Linux executables or libraries at the runtime, without the need for restart of your applications.

LibcarePlus is a self-maintained branch derived from the LibCare project. It is released with the same license with LibCare and the original copyright is kept.

More information about LibCare can be reached at:

[https://github.com/cloudlinux/libcare](https://github.com/cloudlinux/libcare)

LibcarePlus aims at providing user-mode live patches for popular architectures like x86_64 and AArch64.

#### Software Architecture

For more information about the design of LibcarePlus, see the `LibcarePlus Design`.

#### Installation

Refer to the `LibcarePlus Intallation Guide` to install LibcarePlus.


#### User Guide

Refer to:

* LibcarePlus Patch Make Tutorial
* LibcarePlus Patch Apply Tutorial

to help you make the patch and apply it to the target process.

#### Contribution

Any contributors are welcome to join this project, and we are glad to provide help and guidance needed.
Developers can post an issue on this project and submit a pull request to join the development process.